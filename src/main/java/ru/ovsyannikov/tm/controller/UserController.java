package ru.ovsyannikov.tm.controller;

import ru.ovsyannikov.tm.entity.User;
import ru.ovsyannikov.tm.service.UserService;

import java.util.Arrays;
import java.util.List;
import static ru.ovsyannikov.tm.constant.TerminalConst.ROLE_USER;


public class UserController extends AbstractController {

    private final UserService userService;

    public User currentUser;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        System.out.println("[PLEASE, ENTER password:]");
        final String password = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME:]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER LAST NAME:]");
        final String lastName = scanner.nextLine();

        if (userService.createUser(login, password, ROLE_USER, firstName, lastName) == null) {
            System.out.println("[ERROR! CHOOSE DIFFERENT LOGIN]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateUser(login, password, firstName, lastName);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[CLEAR USER BY LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) {
            System.out.println("[USERS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getLogin());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listUser() {
        System.out.println("[LIST USER]");
        viewUsers(userService.findAll());
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ENTER LOGIN: ");
        final User user = userService.findByLogin(scanner.nextLine());
        viewUser(user);
        return 0;
    }

    public int logOn() {
        System.out.println("ENTER LOGIN: ");
        final User user = userService.findByLogin(scanner.nextLine());
        if (user == null) {
            System.out.println("LOGIN IS NOT FOUND IN SYSTEM.");
            return 0;
        }
        System.out.println("ENTER PASSWORD: ");
        final String password = userService.useMD5(scanner.nextLine());
        if (user.getPassword().equals(password)) {
            System.out.println("WELCOME, " + user.getLogin());
            currentUser = user;
        } else {
            System.out.println("FAIL");
        }
        return 0;
    }

    public int checkAuthorisation(final String param) {
        if (currentUser == null) {
            System.out.println("PLEASE, REGISTRY.");
            return -1;
        }
        if (!Arrays.asList(currentUser.getRole()).contains(param)) {
            System.out.println("NOT ALLOWED.");
            return -1;
        }
        return 0;
    }

    public int logOff() {
        currentUser = null;
        System.out.println("LOG OFF");
        return 0;
    }

}
