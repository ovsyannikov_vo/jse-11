package ru.ovsyannikov.tm.controller;

public class SystemController extends AbstractController {

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

    public int displayAbout() {
        System.out.println("Ovsyannikov Vladislav");
        System.out.println("vldslv.ovsyannikov@gmail.com");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.6");
        return 0;
    }

    public int displayHelp() {
        System.out.println("registry - Log on.");
        System.out.println("log-off - Log off.");
        System.out.println();
        System.out.println("user-create - Create user.");
        System.out.println("user-clear - Clear list of users.");
        System.out.println("user-list - Display list of users.");
        System.out.println("user-view-by-login - Display user by login.");
        System.out.println("user-remove-by-login - Remove user by login.");
        System.out.println("user-update-by-login - Update user by login.");
        System.out.println();
        System.out.println("version - Display application version");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of commands");
        System.out.println("exit - Terminate console application");
        System.out.println();
        System.out.println("project-list - Display list of projects");
        System.out.println("project-view - Display project by index");
        System.out.println("project-create - Create new project by name");
        System.out.println("project-clear - Delete all projects");
        System.out.println("project-remove-by-name - Delete  project by name");
        System.out.println("project-remove-by-id - Delete project by id");
        System.out.println("project-remove-by-index - Delete project by index");
        System.out.println("project-update-by-index - Update project by index");
        System.out.println();
        System.out.println("task-list - Display list of tasks");
        System.out.println("task-view - Display task by index");
        System.out.println("task-create- Create new task by name");
        System.out.println("task-clear - Delete all tasks");
        System.out.println("task-remove-by-name - Delete  task by name");
        System.out.println("task-remove-by-id - Delete task by id");
        System.out.println("task-remove-by-index - Delete task by index");
        System.out.println("task-update-by-index - Update task by index");
        System.out.println("tasks-list-by-project-id - Display list of task from project by project_id");
        System.out.println("task-add-to-project-by-ids - Add task to project");
        System.out.println( "task-remove-from-project-by-ids - Remove task from project");
        return 0;
    }

}
