package ru.ovsyannikov.tm.service;

import ru.ovsyannikov.tm.entity.User;
import ru.ovsyannikov.tm.repository.UserRepository;
import java.util.List;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String useMD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.getMessage();
        }
        return null;
    }

    public User createUser(String login, String password, String[] role, String firstName, String lastName) {
        if (login == null) return null;
        if (password == null) return null;
        if (role == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.createUser(login, useMD5(password), role, firstName, lastName);
    }

    public User findByLogin(String login) {
        if (login == null) return null;
        return userRepository.findByLogin(login);
    }

    public User updateUser(String login, String password, String firstName, String lastName) {
        if (login == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateUser(login, useMD5(password), firstName, lastName);
    }

    public User removeByLogin(String login) {
        if (login == null) return null;
        return userRepository.removeByName(login);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }


}

