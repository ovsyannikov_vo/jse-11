package ru.ovsyannikov.tm.repository;

import ru.ovsyannikov.tm.entity.User;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private List<User> users = new ArrayList<>();

    public User createUser(final String login, final String password, final String[] role, final String firstName,
                       final String lastName) {
        User user = findByLogin(login);
        if (user == null) {
            user = new User(login, password, role, firstName, lastName);
            users.add(user);
        }
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User updateUser(final String login, final String password, final String firstName, final String lastName) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User removeByName(final String login) {
        User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }


}
