package ru.ovsyannikov.tm;

import ru.ovsyannikov.tm.controller.ProjectController;
import ru.ovsyannikov.tm.controller.SystemController;
import ru.ovsyannikov.tm.controller.TaskController;
import ru.ovsyannikov.tm.controller.UserController;
import ru.ovsyannikov.tm.repository.ProjectRepository;
import ru.ovsyannikov.tm.repository.TaskRepository;
import ru.ovsyannikov.tm.repository.UserRepository;
import ru.ovsyannikov.tm.service.ProjectService;
import ru.ovsyannikov.tm.service.ProjectTaskService;
import ru.ovsyannikov.tm.service.TaskService;
import ru.ovsyannikov.tm.service.UserService;

import java.util.Arrays;
import java.util.Scanner;

import static ru.ovsyannikov.tm.constant.TerminalConst.*;

public class Application {

    /*
        Task-Manager
        version: 1.0.6
        developer: Ovsyannikov Vladislav
    */
    
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final UserRepository userRepository = new UserRepository();

    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final UserService userService = new UserService(userRepository);

    private final ProjectController projectController = new ProjectController(projectService, projectTaskService);
    private final TaskController taskController = new TaskController(taskService, projectTaskService);
    private final SystemController systemController = new SystemController();
    private final UserController userController = new UserController(userService);

    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("DEMO TASK 1");
        taskRepository.create("DEMO TASK 2");
        userService.createUser("admin", "qwerty", ROLE_ADMIN, "fname1", "sname1");
        userService.createUser("test", "11111", ROLE_USER, "fname2", "sname2");

    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application application = new Application();
        application.run(args);
        application.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            application.run(command);
        }
    }

    public void run(final String[] args) {
        if (args.length == 0) return;
        if (args.length <1) return;
        final String param=args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        if (!Arrays.asList(ROLE_ALL).contains(param)) {
            if (userController.checkAuthorisation(param) == -1) return -1;
        }

        switch (param){
            case LOG_ON: return userController.logOn();
            case LOG_OFF: return userController.logOff();

            case USER_CREATE: return userController.createUser();
            case USER_CLEAR: return userController.clearUser();
            case USER_LIST: return userController.listUser();
            case USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();

            case VERSION: return systemController.displayVersion();
            case ABOUT:  return systemController.displayAbout();
            case HELP: return systemController.displayHelp();
            case EXIT: return systemController.displayExit();

            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectByID();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_CREATE: return taskController.createTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_LIST: return taskController.listTask();
            case TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskByID();
            case TASK_REMOVE_BY_INDEX: return  taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return  taskController.updateTaskByIndex();
            case TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskFromProjectByIds();
            case TASK_LIST_BY_PROJECT_ID: return taskController.listTaskByProjectId();

            default: return systemController.displayError();
        }
    }

    public TaskService getTaskService() {
        return taskService;
    }
    public ProjectService getProjectService() {
        return projectService;
    }
    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
    public UserService getUserService() { return userService; }


}
